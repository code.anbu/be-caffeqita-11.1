<?php
    include '../config/connection.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        if (trim($_POST['kode_transaksi']) == "") {
            echo json_encode(array(
                "message" => "Kode transaksi belum diisi!",
                "code" => 400,
                "status" => false));
        } else {
            $kode_transaksi = $_POST['kode_transaksi'];

            $transaksi_today = "SELECT

            tbl_transaksi.kode_transaksi, tbl_pelanggan.nama_pelanggan, SUM(tbl_menu.harga_menu*tbl_detailtransaksi.jumlah_pesanan) 'Total Bayar', tbl_transaksi.metode_pembayaran, tbl_transaksi.total_cash 'Total Cash', tbl_transaksi.total_cash-SUM(tbl_menu.harga_menu*tbl_detailtransaksi.jumlah_pesanan) 'Uang Kembalian' 
            
            FROM tbl_detailtransaksi
            JOIN tbl_transaksi ON tbl_detailtransaksi.kode_transaksi=tbl_transaksi.kode_transaksi
            JOIN tbl_menu ON tbl_detailtransaksi.kode_menupesanan=tbl_menu.kode_menupesanan
            JOIN tbl_pelanggan ON tbl_transaksi.kode_pelanggan=tbl_pelanggan.kode_pelanggan
            
            WHERE tbl_transaksi.kode_transaksi = '$kode_transaksi'";

            $exec_transaksi_today = mysqli_query($_AUTH, $transaksi_today);

            $_response = array();
            
            while ($row = mysqli_fetch_array($exec_transaksi_today)) {
                array_push($_response, array(
                    'kode_transaksi' => $row[0],
                    'nama_pelanggan' => $row[1],
                    'total_bayar' => $row[2],
                    'metode_pembayaran' => $row[3],
                    'total_cash' => $row[4],
                    'uang_kembalian' => $row[5]
                    )
                );
                echo json_encode(array(
                    "message" => "Data dengan account username $kode_transaksi TERSEDIA di database.",
                    "code" => 200,
                    "status" => true,
                    "detail_transaksi" => $_response));
                mysqli_close($_AUTH);
            }
        }
    } else {
        $_response["message"] = trim("Forbiedden.");
        $_response["code"] = 400;
        $_response["status"] = false;

        echo json_encode($_response);
    }
?>